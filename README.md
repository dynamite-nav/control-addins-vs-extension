# Dynamite Central : Control Addins VS Extension

[![Download Stable Latest](https://img.shields.io/badge/Download-Latest%20Stable-brightgreen)](https://gitlab.com/dynamite-central/control-addins-vs-extension/-/jobs/artifacts/main/download?job=build_vsix)

[![Download Development Latest](https://img.shields.io/badge/Download-Latest%20Development-orange)](https://gitlab.com/dynamite-central/control-addins-vs-extension/-/jobs/artifacts/dev/download?job=build_vsix)

## Develop your first addin with the  web modern development

First, you must be in a working workspace to be able to execute the commands. If so, open your explorer (SHIFT + P) and run the command with the template you want to use :

![alt text](/documentations/screenshot-1.png "Title")

You will be asked for the installation directory. The name of your addin is also the name of the directory where the files are installed. 

![alt text](/documentations/screenshot-2.png "Title")

Be careful, if you later change the directory name, it may compromise the build and you will have to manually change the relative path in the files.

Once you've created your addin, a popup will appear to let you know that the operation was successful, and you can initialize the project.

![alt text](/documentations/screenshot-3.png "Title")

The button "Init Project" generates a new terminal for your addin, and automatically launches the command "npm install", then "npm run build".  At this point, you can already use your addin on a AL page to see the result.


![alt text](/documentations/screenshot-4.png "Title")

Develop your frontend with "npm run dev", build your files with "npm run build", and that's all. You don't need to update your ControlAddin.al manualy.


