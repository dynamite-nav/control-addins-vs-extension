import * as vscode from 'vscode';

//TODO : maybe later to finish propuse of differents basic addins...
export function addCompletionForUserControlField(context: vscode.ExtensionContext)
{
    const provider = vscode.languages.registerCompletionItemProvider(
		'al',
		{
			provideCompletionItems(document, position) {

				// get all text until the `position` and check if it reads `console.`
				// and if so then complete if `log`, `warn`, and `error`
				const linePrefix = document.lineAt(position).text.slice(0, position.character);
				const regex = /usercontrol\(\s*(".*?"|\w*)\s*;$/;
                if (!regex.test(linePrefix)) {
                    return undefined;
                }

				return [
					new vscode.CompletionItem('color picker', vscode.CompletionItemKind.Event),
					new vscode.CompletionItem('mask input', vscode.CompletionItemKind.Method),
					new vscode.CompletionItem('rich text editor', vscode.CompletionItemKind.Method),
				];
			}
		},
		';' 
	);

    context.subscriptions.push(provider);
}