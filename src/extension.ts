import * as vscode from 'vscode';
import {addCompletionForUserControlField} from './completionItemProvider';
import addTemplateCommands from './templatesCommands';


export function activate(context: vscode.ExtensionContext) {

	addTemplateCommands(context);
	//addCompletionForUserControlField(context);

}

// This method is called when your extension is deactivated
export function deactivate() {}
