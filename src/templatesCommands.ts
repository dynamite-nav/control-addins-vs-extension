import * as vscode from 'vscode';
import * as path from 'path';
import * as os from 'os';
import * as fs from 'fs-extra';
import * as ejs from 'ejs';

export default function addTemplates(context: vscode.ExtensionContext)
{
    addTemplateCommand(context,"dynamitecentral.controladdin.new.templatehtml",async(destFolder,rootPath,variables)=>{
        
        variables.vue = false;
        const templateDir = path.join(context.extensionPath, 'template');
        await copyAndProcessTemplates(templateDir, destFolder, variables);
    });
    addTemplateCommand(context,"dynamitecentral.controladdin.new.templatevuejs",async(destFolder,rootPath,variables)=>{
        
        variables.vue = true;
        const templateDir = path.join(context.extensionPath, 'template');
        await copyAndProcessTemplates(templateDir, destFolder, variables);
        await copyAndProcessTemplates(path.join(context.extensionPath, 'template_vue'), destFolder, variables);
    });
}   


export function addTemplateCommand(context: vscode.ExtensionContext, commandName : string, func : Function) {
    context.subscriptions.push(vscode.commands.registerCommand(commandName, async () => {
        const destinationFolder = await askDestinationFolder() as string;

        if (!destinationFolder) {
            vscode.window.showErrorMessage('Destination folder was not provided.');
            return;
        }

        const rootPath = vscode.workspace.workspaceFolders?.[0].uri.fsPath;
        if (!rootPath) {
            vscode.window.showErrorMessage('Workspace root path not found.');
            return;
        }

        const variablesEJS = {
            relativePath : path.relative(rootPath, destinationFolder).replace(/\\/g, '/'),
            addinName : path.basename(destinationFolder)
        };

        await func.call(null,destinationFolder,rootPath,variablesEJS);

        const documentUri = vscode.Uri.file(path.resolve(destinationFolder,path.basename(destinationFolder)+'.al'));
        const documentAddin : vscode.TextDocument = await  vscode.workspace.openTextDocument(documentUri);
        await vscode.window.showTextDocument(documentAddin);

        vscode.window
            .showInformationMessage("Your ControlAddin has been created !", ...['Init Project'])
            .then(selection => {
                const terminal = vscode.window.createTerminal(variablesEJS.addinName);
                terminal.show();
                terminal.sendText("cd "+ variablesEJS.relativePath);
                terminal.sendText("npm install");
                terminal.sendText("npm run build");                
        });

    }));
}

async function copyAndProcessTemplates(sourceDir: string, destinationDir: string, variables: { [key: string]: any }) {
    const files = await fs.readdir(sourceDir);

    for (const file of files) {

        const sourceFilePath = path.join(sourceDir, file);
        let destinationFilePath = path.join(destinationDir, file);

        const stats = await fs.lstat(sourceFilePath);
        if (stats.isDirectory()) {
            await fs.ensureDir(destinationFilePath);
            await copyAndProcessTemplates(sourceFilePath, destinationFilePath, variables);
        } else {

            const renderedContent = [".js",".html",".addin",".json",".al",".vue"].includes(path.extname(sourceFilePath)) ? ejs.render(await fs.readFile(sourceFilePath, 'utf8'), variables) : await fs.readFile(sourceFilePath);
                  
            if (file.endsWith(".addin")) {
                destinationFilePath = path.join(destinationDir, variables.addinName + ".al");
            }
            if (file === "index.js") {
                destinationFilePath = path.join(destinationDir, variables.addinName + ".js");
            }

            fs.outputFile(destinationFilePath, renderedContent);
        }
    }
}

async function askDestinationFolder() {
    //TODO : active document path folder before
    const currentlyOpenTabfilePath: string = vscode.workspace.workspaceFolders && vscode.workspace.workspaceFolders.length > 0
        ? vscode.workspace.workspaceFolders[0].uri.fsPath
        : os.homedir();
    const defaultPathFolder = path.join(currentlyOpenTabfilePath, "Addins", "MyControlAddin");
    return await vscode.window.showInputBox({
        prompt: 'Enter the add-in name with complete path folder:',
        value: defaultPathFolder,
        validateInput: (text: string) => {
            if(!text){
                return "Path cannot be empty";
            }            
            return null;
        }
    });
}