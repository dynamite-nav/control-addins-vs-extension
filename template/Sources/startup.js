fetch(Microsoft.Dynamics.NAV.GetImageResource("<%= relativePath %>/Build/Images/Content.html"))
.then(response=> response.text())
.then(rawHtml=> {
  const controlAddin = document.getElementById('controlAddIn');
  controlAddin.innerHTML = rawHtml;
  window.dispatchEvent(new CustomEvent("contentLoaded", { "detail": controlAddin }));

  Microsoft.Dynamics.NAV.InvokeExtensibilityMethod('OnAddInReady', null);
});
