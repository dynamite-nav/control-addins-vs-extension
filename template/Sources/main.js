import viteLogo from '/Images/vite.png';
import * as styles from './style.css';
<% if(vue){ %>import { createApp } from 'vue';
import App from './App.vue'; <% } %>

window.addEventListener("contentLoaded", async(addin) => {
       
    console.log("Your content is loaded, use your DOM functions here !");
    <% if(vue){ %>
    const app = createApp(App);
    app.mount('#app')
    <% }else{ %>
    //Example with an import.
    const imgViteLogo =  document.createElement("img")
    imgViteLogo.title = "example with an import from public folder"
    imgViteLogo.src = viteLogo
    document.getElementById("example").appendChild(imgViteLogo)

    //Example with dynamic loading.
    const img =  document.createElement("img")
    img.title = "examplewith dynamic loading from public folder"
    img.src = await window.getNAVResource('./Images/js.png')
    document.getElementById("example").appendChild(img)
    <% } %>
});