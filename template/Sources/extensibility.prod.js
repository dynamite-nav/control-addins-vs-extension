/* ☕ This file is used in production mode, with all importation converted by the builder. 
So don't be  worry about the unkown class Microsoft.Dynamics.NAV and let operate the magic system ❤
*/
const projectPath = "<%= relativePath %>";

window.getNAVResource = async(path) => {
    const endpoint = path.replace("./","/");
    const request = await fetch(Microsoft.Dynamics.NAV.GetImageResource( projectPath+"/Build"+endpoint));
    if (!request.ok){
        throw new Error(`HTTP error! status: ${request.status}`);
    }
    const contentType = request.headers.get('Content-Type')
    if (contentType.includes('text')) {
        return await request.text();
    } else if (contentType.includes('application/json')) {
        return await request.json();
    } else {
        const blob =  await request.blob();
        return URL.createObjectURL(blob);
    }
};

window.getNAVUrl = (path) =>{
    const endpoint = path.replace("./","/");
    return Microsoft.Dynamics.NAV.GetImageResource( projectPath+"/Build"+endpoint)
}