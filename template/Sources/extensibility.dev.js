// ☕ This file is used in development mode, with injection in the background. 


// You need to use this function "getNAVResource" to import your assets. 
window.getNAVResource = async (path) => {
    try {
        const module = await import( /* @vite-ignore */path);
        return module.default;
    } catch (error) {
        console.error(`Failed to load module at ${path}:`, error);
        throw error;
    }
};


window.getNAVUrl = (path) => path;