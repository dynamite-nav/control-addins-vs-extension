import { defineConfig } from 'vite';
import fs from 'fs';
import esbuild from 'esbuild';
import * as path from 'path';
<% if(vue) { %>import vue from '@vitejs/plugin-vue';<% } %>


const relativePathFolder = "<%= relativePath %>";
const addinName = "<%= addinName %>";
const buildDir = "Build";

export default defineConfig(({ mode }) => {

  return {
    publicDir: 'public',
    root: 'Sources',
    build: {
      emptyOutDir: true,
      outDir: path.join('..',buildDir),
      rollupOptions: {
        output: {
          entryFileNames: `Scripts/[name]-[hash].js`,
          chunkFileNames: `Scripts/[name]-[hash].js`,
          assetFileNames: assetInfo => {
            const name = path.basename(assetInfo.name, path.extname(assetInfo.name));
            const extType = assetInfo.name.split('.').pop();
            if (extType === 'css') { return `Styles/[name].[ext]`; } else {
              return `Images/[name].[ext]`;
            }
          },
        },
      },
    },
    plugins: [
      <% if(vue) { %>vue(),<% } %>
      ControlAddinDevMode(),
      ControlAddinTranspiler(),
      ControlAddinRewriterAlObject(),
    ]
  }
})

/* ◈ The ControlAddinDevMode functions are :
-  inject the extensibility.dev.js script 
-  dispatch the event "contentLoaded" */
function ControlAddinDevMode() {
  return {
    name: 'controladdin-dev-mode',
    apply: 'serve',
    transformIndexHtml(html) {
      const bodyEndIndex = html.lastIndexOf('</body>');
      const injectCode = '<script type="module" src="extensibility.dev.js"></script><script type="module">window.dispatchEvent(new CustomEvent("contentLoaded", { "detail": document.getElementById("controlAddIn") }))</script>'
      return html.slice(0, bodyEndIndex) + injectCode + html.slice(bodyEndIndex);
    },

  }
}

/* ◈ The ControlAddinTranspiler functions are :
- change all import with window.getNAVResource 
- copy the extra files : startup.js, extensibility.prod.js
- keep only from the body html of index.html and change the name for Content.html 
 */
function ControlAddinTranspiler() {
  return {
    name: 'controladdin-transpiler',
    apply: 'build',
    transform(code,id){
      if (!id.endsWith('.js') && !id.endsWith('.ts')) return null;

      const regex = new RegExp(`import\\s+(\\w+)\\s+from\\s+['"](/Images[^'"]+)['"]`, 'g');
      const modifiedCode = code.replaceAll(regex, (match, p1, p2) => {

          return `const ${p1} = window.getNAVUrl('${p2}')`;
      });
      return {
        code: modifiedCode,
        map: null
      };
    },
    async generateBundle(options, bundle) {
      /*
      for (const fileName in bundle) {
        const asset = bundle[fileName];
        if (asset.type === 'asset' && /\.(png|jpe?g|gif|svg)$/.test(asset.fileName)) {
            const publicDir = path.resolve(__dirname, 'public');
            const targetPath = path.join(publicDir, asset.fileName);
            fs.mkdirSync(path.dirname(targetPath), { recursive: true });
            fs.writeFileSync(targetPath, asset.source);
        }*/
       
      for (const file of ['startup.js','extensibility.prod.js']) {
        try {
          const filePath = path.resolve(__dirname, 'Sources', file);
          const code = await fs.promises.readFile(filePath, 'utf-8');
          const result = await esbuild.transform(code, {
            loader: 'js',
            target: 'es2015',
            minify: true
          })
          this.emitFile({
            type: 'asset',
            fileName: path.join('Scripts',file),
            source: result.code
          })
        } catch (error) {
          this.error(`Failed to transpile ${path} : ${error}`)
        }
      }
    },
    transformIndexHtml(html) {
      const regex = /<body.*?>([\s\S]*?)<\/body>/im;
      const match = regex.exec(html);
      return match == null ? html : match[1];
    },
    async closeBundle() {
      const htmlFilePath = path.resolve(buildDir, 'index.html');
      const code = await fs.promises.readFile(htmlFilePath, 'utf-8');
      const desContent = path.resolve(buildDir, "Images", "Content.html");
      if (!fs.existsSync(path.resolve(buildDir, "Images"))) {
        fs.mkdirSync(path.resolve(buildDir, "Images"));
      }
      await fs.promises.writeFile(desContent, code);
      await fs.promises.unlink(htmlFilePath);
    }
  }
}

/* ◈ ControlAddinRewriterAlObject functions are :
- Read the content builded, and write the files path to the control addin file
- By default, the Content.html is already written with every build.
*/
function ControlAddinRewriterAlObject() {
  return {
    name: 'controladdin-rewriter-al',
    apply: 'build',
    async generateBundle(options, bundle) {
      try {
        const controlAddinPath = path.resolve(__dirname, addinName+'.al');
        const alFile = await fs.promises.readFile(controlAddinPath, 'utf-8');
        
        let scripts = [path.join(relativePathFolder,"Build","Scripts","extensibility.prod.js")]
        let styles = [];
        let images = [];
        const invalidFileNames = ['extensibility.prod.js','startup.js', 'prevent.js'];

        for (const [fileName] of Object.entries(bundle)) {
          if (fileName.endsWith('js') && !invalidFileNames.find(e => fileName.endsWith(e))) {
            scripts.push(path.join(relativePathFolder,buildDir,fileName))
          } else if (fileName.endsWith('css')) {
            styles.push(path.join(relativePathFolder,buildDir,fileName))
          } else {
            if (fileName.endsWith(".html")) {
              images.push(path.join(relativePathFolder,buildDir, fileName))
            }
            if(!invalidFileNames.find(e => fileName.endsWith(e))){
              images.push(fileName)
            }
          }
        }

        function listFiles(dir) {
          let results = [];
          const files = fs.readdirSync(dir);
        
          files.forEach(file => {
            const filePath = path.join(dir, file);
            const stats = fs.statSync(filePath);
            
            if (stats.isFile()) {
              images.push(path.join(relativePathFolder,buildDir,"Images",file))
            } else if (stats.isDirectory()) {
              results = results.concat(listFiles(filePath));
            }
          });       
          return results;
        }
        listFiles(path.join(__dirname, buildDir,'Images'));

        images.push(path.join(relativePathFolder,buildDir, "Images", "Content.html"));
      

        const updatedScripts = alFile.replace(/(Scripts = )[^;]*(;)/g, "Scripts = " + scripts.map(e => "'" + e + "'").join(',\n\t') + ";");
        const updatedStyles = updatedScripts.replace(/(StyleSheets = )[^;]*(;)/g, "StyleSheets = " + styles.map(e => "'" + e + "'").join(',') + ";");
        const updatedImages = updatedStyles.replace(/(Images = )[^;]*(;)/g, "Images = " + images.map(e => "'" + e + "'").join(',\n\t') + ";");

        await fs.promises.writeFile(controlAddinPath, updatedImages, 'utf8');

      } catch (e) {
        this.error(`Failed to write AL file: ${e}`);
      }
    }
  }
}
